FROM node:latest as builder
# 设置容器内工作目录
WORKDIR /usr/src/app
COPY package.json .
RUN npm install
COPY . .
RUN npm run build


# 暴露容器内部应用监听的端口（如果需要）
EXPOSE 1001

RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
# 设置容器为北京时间，方便查看日志

# 启动应用
CMD ["npm","run", "serve"]