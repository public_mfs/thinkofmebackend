import express from 'express';
const app = new express();
const port = 1001;

function serverInit() {
    app.use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
        next();
    });
    app.use(express.json());//避免解析req.body为undefined，也可以使用body-parser等中间件
    app.get('/', (req, res) => {
        res.send('Hello World');
    });
    app.post('/sendEmail', async function (req, res) {
        const subject = decodeURIComponent(req.body.subject) || '测试标题';
        const text = decodeURIComponent(req.body.text) || '测试内容';
        res.setHeader('Content-Type', 'application/json');
        try {
            // 调用 sendEmail 函数发送邮件
            await sendEmail(subject, text);
            // 发送成功，返回状态码 200 和 success 设置为 true
            console.log("成功了");
            res.status(200).json({ success: true });
        } catch (error) {
            console.log("失败了");
            // 发送失败，返回状态码 500 和 success 设置为 false
            res.status(500).json({ success: false, extra: JSON.stringify(error) });
        }
    })
}


function serverStart() {
    app.listen(port, () => {
        console.log(`服务已经运行在${port}端口上！`);
    });
}

import nodemailer from "nodemailer";
let transporter = nodemailer.createTransport({
    host: 'smtp.qq.com',//使用qq发送邮件
    // port: 587,
    // secure: false,
    auth: {
        user: "2984364143@qq.com",
        pass: "qwiuyzznmflfdedc",
    },
})
// 邮件服务器准备
transporter.verify(function (error, success) {
    if (error) {
        console.log(error);
    } else {
        console.log('email is ready');
    }
})
function sendEmail(subject = '标题测试', text = '内容测试') {
    return transporter.sendMail({
        from: '2984364143@qq.com',
        to: '1084305889@qq.com',
        subject: subject,
        text: text,
    });
}


serverInit();
serverStart();